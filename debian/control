Source: irstlm
Section: science
Priority: optional
Maintainer: Debian Science Maintainers <debian-science-maintainers@lists.alioth.debian.org>
Uploaders: Giulio Paci <giuliopaci@gmail.com>,
           Koichi Akabe <vbkaisetsu@gmail.com>
Build-Depends: dh-autoreconf,
 devscripts,
 libtool,
 automake,
 autoconf,
 bash-completion,
 debhelper (>= 9~),
 dh-buildinfo,
 zlib1g-dev,
 dpkg-dev (>= 1.16.1~)
Standards-Version: 3.9.8
Vcs-Git: https://anonscm.debian.org/git/debian-science/packages/irstlm.git
Vcs-Browser: https://anonscm.debian.org/git/debian-science/packages/irstlm.git
Homepage: http://hlt-mt.fbk.eu/technologies/irstlm

Package: irstlm
Architecture: any
Depends: libirstlm1 (= ${binary:Version}),
	 ${shlibs:Depends},
	 ${misc:Depends},
	 ${perl:Depends},
	 perl
Description: IRST Language Modeling Toolkit
 The IRST Language Modeling Toolkit can be used to learn a language model
 from data. The generated n-gram models should be usable on any system
 supporting ARPA language model format.
 .
 This package provides the command line tools.

Package: libirstlm-dev
Section: libdevel
Architecture: any
Depends: libirstlm1 (= ${binary:Version}),
         ${misc:Depends}
Description: IRST Language Modeling Toolkit - development files
 The IRST Language Modeling Toolkit can be used to learn a language model
 from data. The generated n-gram models should be usable on any system
 supporting ARPA language model format.
 .
 This package provides development headers for IRSTLM.

Package: libirstlm1
Section: libs
Architecture: any
Multi-Arch: same
Pre-Depends: ${misc:Pre-Depends}
Depends: ${shlibs:Depends}, ${misc:Depends}
Description: IRST Language Modeling Toolkit - runtime library
 The IRST Language Modeling Toolkit can be used to learn a language model
 from data. The generated n-gram models should be usable on any system
 supporting ARPA language model format.
 .
 This package contains the IRSTLM shared library.
